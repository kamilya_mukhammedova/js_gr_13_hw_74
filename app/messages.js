const express = require('express');
const fs = require('fs');
const {nanoid} = require('nanoid');
const router = express.Router();
const path = 'messages';
const allMessages = [];

router.get('/', (req, res) => {
  let lastFiveFiles = [];
  let messagesArray = [];
  const getMessages = (array, limitNumber) => {
    for(let i = 0; i < array.length; i++) {
      const messagePath = path + '/' + array[i];
      try {
        const message = fs.readFileSync(messagePath);
        messagesArray.push(JSON.parse(message));
        if(messagesArray.length === limitNumber) {
          return res.send(messagesArray);
        }
      } catch (e) {
        messagesArray = [];
      }
    }
  };
  fs.readdir(path, (err, files) => {
    if(files.length > 5) {
      lastFiveFiles = files.slice(files.length - 5);
      getMessages(lastFiveFiles, 5);
    } else {
      lastFiveFiles = files.slice();
      getMessages(lastFiveFiles, lastFiveFiles.length);
    }
  });
});

router.get('/:id', (req, res) => {
  const message = allMessages.find(message => message.id === req.params.id);
  if(!message) {
    return res.status(404).send({message: 'Message is not found'});
  }
  return res.send(message);
});

router.post('/', (req, res) => {
  const currentDate = new Date().toISOString();
  const id = nanoid();
  const message = {
    message: req.body.message,
    currentDate
  };
  const fileName = `${currentDate}.txt`;
  fs.writeFile(`messages/${fileName}`, JSON.stringify(message), (err) => {
    if(err) {
      console.log(err);
    } else {
      console.log('File was saved!');
    }
  });
  message.id = id;
  allMessages.push(message);
  return res.send('Created new message with ID = ' + id);
});

module.exports = router;



